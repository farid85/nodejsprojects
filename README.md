# PCAT Project

🔆✅🔴**Live [PCAT](https://pcat-proje.herokuapp.com)**

In the project directory, you can run:

#### Install packages: `npm`

#### Start server: `npm i`

#### Test project: `npm start dev`

Runs the app in the development mode.\
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

#### Used technologies:

##### `✅ Node.js` `✅ Express.js` `✅ Css`
